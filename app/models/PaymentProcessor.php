<?php

class PaymentProcessor {
	
	function startPayment($amount, $returnUrl, $cancelUrl, $currency = "NZD")
	{
		$purchase = Omnipay::purchase([
		'amount'		=> $amount,
		'currency'		=> $currency,
		//'EmailAddress'	=> 'kategriffithsphotography@gmail.com',
		//'transactionId' => uniqid(microtime()),
		//'description' 	=> 'lololol',
		'returnUrl'		=> $returnUrl,
		'cancelUrl'		=> $cancelUrl,
	])->send();

	//return omnipay_debug($purchase);
	if ( ! $purchase->isRedirect()) {
		throw new UnexpectedValueException("Payment gateway responded:\n ". $purchase->getMessage());
		
	}

	return $purchase->getRedirectUrl();

	}

	function isPaymentAccepted()
	{
		$confirm = Omnipay::completePurchase([
			'result' => Input::get('result')
		])->send();
		//dd($confirm);		
		return $confirm->isSuccessful();
	}

}