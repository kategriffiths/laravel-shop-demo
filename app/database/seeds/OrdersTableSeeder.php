<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		DB::table('orders')->truncate();
		DB::table('orders_products')->truncate();

		foreach(range(1, 10) as $index)
		{
			$order = Order::create([
				'user_id' 			=> $faker->numberBetween(1,10),
				'shipping_address' 	=> $faker->address,
				'billing_address' 	=> $faker->address,
				'order_status' 		=> $faker->randomElement(['placed', 'confirmed', 'shipped']),
				'payment_status' 	=> $faker->randomElement(['unpaid', 'paid', 'refunded']),
				'void' 				=> 0
			]);


			foreach(range(1, $faker->numberBetween(1,5)) as $product_index)
			{
				$order->products()->attach(
					$faker->numberBetween(1,20), //random product id
					[
						'quantity' => $faker->numberBetween(1,7)
					]
				); 
			}
		}
	}

}