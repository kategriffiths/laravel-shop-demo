@extends('layouts.master')

@section('page-title')
@overwrite

@section('content')

<div class="container">
	{{ Form:: open(['route' => '/', 'class' => 'form-inline']) }}
		<h2>Sign Up</h2>
		{{ Form::label('username', 'Username') }}
		{{ Form::text()}}		
		
		{{ Form::submit('Sign Up', ['class' => btn btn-success])}}
	{{  Form::close }}

</div>	
  
  <hr>

  <footer>
    <p>&copy; Company 2014</p>
  </footer>
</div> <!-- /container -->

@stop
