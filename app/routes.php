<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function()
{
	return View::make('home');
}]);

Route::get('cart', ['as' => 'cart.index', 'uses' => 'CartController@index']);
Route::post('cart', ['as' => 'cart.store', 'uses' => 'CartController@store']);
Route::match(['put','patch'], 'cart', ['as' => 'cart.update', 'uses' => 'CartController@update']);
Route::get('checkout', ['as' => 'cart.checkout', 'uses' => 'CartController@checkout']);

Route::get('pay',
	['as' => 'cart.pay',
	 'uses' => 'CartController@sendToPaymentProcessor'
]);

Route::get('pay-success',
	 ['as' => 'cart.pay-success',
	  'uses' => 'CartController@verifyWithPaymentProcessor'
]);

Route::get('pay-failure',
	 ['as' => 'cart.pay-failure', function (){
	 	return Redirect::route("cart");
}]);

//TODO: Change to POST
Route::resource('products', 'ProductsController');

Route::resource('users', 'UsersController');
Route::get('register', ['as' => 'register', 'uses' => 'UsersController@create']);
Route::get('login', ['as' => 'login', 'uses' => 'UsersController@login']);
Route::post('login', ['as' => 'authenticate', 'uses' => 'UsersController@authenticate']);
Route::get('logout', ['as' => 'logout', 'uses' => 'UsersController@logout']);


Route::resource('users.orders', 'UserOrderController');



Route::get('test-tx', function(){

	$purchase = Omnipay::purchase([
		'amount'		=> '99.95',
		'currency'		=> 'NZD',
		'EmailAddress'	=> 'kategriffithsphotography@gmail.com',
		'transactionId' => uniqid(microtime()),
		'description' 	=> 'lololol',
		'returnUrl'		=> url('test-rx'),
		'cancelUrl'		=> url('test-fail'),
	])->send();

	//return omnipay_debug($purchase);
	if ($purchase->isRedirect()) {
		return Redirect::to($purchase->getRedirectUrl());
	}

	throw new UnexpectedValueException("Payment gateway responded:\n ". $purchase->getMessage());

});

Route::get('test-rx', function (){
	//return "Success";
	$confirm = Omnipay::completePurchase([
		'result' => Input::get('result')
	])->send();
	//dd($confirm);
	return omnipay_debug($confirm);
});


Route::Get('test-fail', function (){
	return "Fail";
});

function omnipay_debug($purchase) {
	$response = new stdClass();
	$response->isSuccessful				= $purchase->isSuccessful();
	$response->isRedirect				= $purchase->isRedirect();
	$response->getMessage 				= $purchase->getMessage();
	$response->getCode 					= $purchase->getCode();
	$response->getTransactionReference  = $purchase->getTransactionReference();
	if ($purchase->isRedirect()) {
	$response->getRedirectUrl 			= $purchase->getRedirectUrl();
	$response->getRedirectData 			= $purchase->getRedirectData();
	}
	$response->getCardReference			= $purchase->getCardReference();

	return json_encode($response);
};








// Populate layouts.master with some common variables
View::composer('layouts.master', function($view) {
	$cartCount = UserProductCart::currentUser()->cartQuantity();
	$view->with('cartCount', $cartCount);
	
	$view->with('currentUser', Auth::user());
});







App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});