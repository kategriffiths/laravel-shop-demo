<?php

class CartController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 * GET /cart
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = UserProductCart::currentUser();

		$total = $products->cartTotal();

		return View::make('cart.index', compact('products', 'total'));
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /cart
	 *
	 * @return Response
	 */
	public function store()
	{
		$user_id = Auth::id();
		$product_id = Input::get('product_id');
		$quantity = Input::get('quantity', 1);

		$data = compact('user_id', 'product_id', 'quantity');

		$validator = Validator::make($data, UserProductCart::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		// find existing matching product in cart
		$cart_entry = UserProductCart::where('user_id', $user_id)->where('product_id', $product_id)->first();

		if ($cart_entry) {
			$cart_entry->quantity += $quantity;
			$cart_entry->save();
		} else {
			$cart_entry = UserProductCart::create(
				compact('user_id', 'product_id', 'quantity')
			);
		}

		return Redirect::route('cart.index');
	}

	/**
	 * Display the specified resource.
	 * GET /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cart/
	 *
	 * @return Response
	 */
	public function update()
	{
		$userCart = UserProductCart::currentUser();
		// loop over all the input fields
		foreach (Input::all() as $key => $quantity) {
			// if the key starts with 'quantity-cart-'
			if (strpos($key, 'quantity-cart-') > -1) {
				// get the id
				$cart_id = explode('-', $key)[2];
				// update the quantity of that product in the user cart
				$cartitem = $userCart->find($cart_id);
				$cartitem->quantity = floor($quantity);
				$cartitem->save();
			}
		}

		return Redirect::route('cart.index');
	}

	public function checkout()
	{
		//collect info for a new order

		//display a form

		//redirect  
	}


	public function sendToPaymentProcessor()
	{
		//validate address fields

		//redirect if invalid

		//copy cart order to a new order
		$cart = UserProductCart::currentUser();

		//this is a collection, so we should be able to ask for a duplicate of the order
		$order = Order::makeFromCart($cart);

		$amount = $cart->cartTotal();

	//save order id to session
		Session::put('order_id', $order->id);

		$processor = new PaymentProcessor;
		$url = $processor->startPayment($amount, url('pay-success'), url('pay-failure'));
		

		//redirect to DPS
		return Redirect::to($url);
		

		

	}

	public function verifyWithPaymentProcessor()
	{

		//Check if DPS says its accepted or not
		$processor = new PaymentProcessor;
		$accepted = $processor->isPaymentAccepted(Input::get('result'));
		if (!$accepted) {
			return Redirect::route('cart.pay-failure');
		}
		//load order from session order id
		$order = Order::findOrFail(Session::get('order_id'));
		
		//Update order to mark as paid and confirmed
		$order->payment_status = 'paid';
		$order->save();
		
		//Tell the user
		return Redirect::route("users.orders.show", [$order->user_id, $order->id]);
	}








	/**
	 * Remove the specified resource from storage.
	 * DELETE /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}