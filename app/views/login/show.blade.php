@extends('layouts.master')

@section('content')

<div class="container">
	{{ Form:: open(['route' => 'products.show', 'class' => 'form-inline']) }}
		<h2>Sign Up</h2>
		{{ Form::label('username', 'Username') }}
		{{ Form::text()}}		
		
		{{ Form::submit('Sign Up', ['class' => btn btn-success])}}
	{{  Form::close }}

</div>	
  
  <hr>

  <footer>
    <p>&copy; Company 2014</p>
  </footer>
</div> <!-- /container -->

@stop