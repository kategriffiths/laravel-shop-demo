<?php

class UserOrderController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($user_id)
	{
		$orders = Order::where('user_id', $user_id)->get();
		return $orders;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($user_id)
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($user_id)
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $order_id,
	 * @return Response
	 */
	public function show($user_id, $order_id)
	{
		$order = Order::findOrFail($order_id);
		$products = $order->products;

		return $products;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $order_id,
	 * @return Response
	 */
	public function edit($user_id, $order_id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $order_id,
	 * @return Response
	 */
	public function update($user_id, $order_id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $order_id,
	 * @return Response
	 */
	public function destroy($user_id, $order_id)
	{
		//
	}


}
