<?php

return array(

	// The default gateway to use
	'default' => 'pxpay',

	// Add in each gateway here
	'gateways' => array(
		'paypal' => array(
			'driver' => 'PayPal_Express',
			'options' => array(
				'solutionType' => '',
				'landingPage' => '',
				'headerImageUrl' => ''
			)
		),
		'pxpay' => array(
			'driver' => 'PaymentExpress_PxPay',
			'options' => array(
				'username' => getenv('PXPAY_USERNAME'),
				'password' => getenv('PXPAY_PASSWORD'),
				'solutionType' => '',
				'landingPage' => '',
				'headerImageUrl' => ''
			)
		)
	)
);